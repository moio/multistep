# Multiform

## Installation

### docker-compose

```bash
docker-compose up -d --build
docker-compose up
```

### npm

```python
npm install
npm run start
```

### yarn

```python
yarn
yarn start
```

## Usage
1. first step
    * firstname: `optional`
    * username: jo
    * email: jo@example.com
2. second step
    * url: `optional` or example.com
3. third step
    * password: secret
