import { Children, cloneElement } from 'react';

export const randBool = () => Math.random() < 0.8;

export const stall = async (value) => {
	const delay = (ms) => new Promise((res) => setTimeout(res, ms));
	await delay(2000);
	return value;
};

export const range = (num) => {
	return [...Array(num).keys()];
};

export const toObject = (args, getValue) =>
	args.reduce((acc, cur) => {
		acc[cur] = getValue(cur);
		return acc;
	}, {});

export const controlledInputTrasform = (child, onChange, getValue) => {
	const name = child.props.name;
	const value = getValue(name);
	return cloneElement(child, {
		onChange: (e) => onChange(name, e.target.value),
		value: value || '',
	});
};

export const toControlledSteps = (steps, onChange, getValue) =>
	Children.map(steps, (child) => {
		return cloneElement(child, {
			children: Children.map(child.props.children, (child) => {
				return child.type === 'input'
					? controlledInputTrasform(child, onChange, getValue)
					: child;
			}),
		});
	});
