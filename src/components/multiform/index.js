import { useState } from 'react';
import {
	CurrentStepContext,
	SetCurrentStepContext,
	SetStepStatusContext,
	StepStatusContext,
} from '../../context';
import { toControlledSteps } from '../../utils';
import Horizontalstepper from '../horizontalstepper';
import Verticalstepper from '../verticalstepper';
import './style.css';

const Multiform = ({ steps, validationSteps, onDone }) => {
	const [form, setForm] = useState({});
	const [stepsStatus, setStepStatus] = useState([]);
	const [currentStep, setCurrentStep] = useState(0);

	const updateForm = (name, value) => setForm({ ...form, [name]: value });
	const getFormValue = (name) => form[name];

	const controlledSteps = toControlledSteps(steps, updateForm, getFormValue);

	return (
		<>
			<StepStatusContext.Provider value={stepsStatus}>
				<SetStepStatusContext.Provider
					value={(statuses) => setStepStatus(statuses)}
				>
					<CurrentStepContext.Provider value={currentStep}>
						<SetCurrentStepContext.Provider
							value={(step) => setCurrentStep(step)}
						>
							<Verticalstepper
								className="multiform--isVertical"
								validationSteps={validationSteps}
								steps={controlledSteps}
								onDone={onDone}
							/>
							<Horizontalstepper
								className="multiform--isHorizontal"
								validationSteps={validationSteps}
								steps={controlledSteps}
								onDone={onDone}
							/>
						</SetCurrentStepContext.Provider>
					</CurrentStepContext.Provider>
				</SetStepStatusContext.Provider>
			</StepStatusContext.Provider>
		</>
	);
};

export default Multiform;
