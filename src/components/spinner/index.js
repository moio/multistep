import './style.css';

const Spinner = ({ show }) => {
	const showHideClassName = show ? 'modal modal--isVisible' : 'modal modal--isHidden';

	return (
		<div className={showHideClassName}>
			<section className="modal__main">
				<div className="spinner" />
			</section>
		</div>
	);
};

export default Spinner;
