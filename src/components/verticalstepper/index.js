import { useContext, useEffect } from 'react';
import {
	CurrentStepContext,
	SetCurrentStepContext,
	SetStepStatusContext,
	StepStatusContext,
} from '../../context';

const Verticalstepper = ({ className, onDone, validationSteps, steps }) => {
	const setStepStatus = useContext(SetStepStatusContext);
	const stepsStatus = useContext(StepStatusContext);
	const setCurrentStep = useContext(SetCurrentStepContext);
	const currentStep = useContext(CurrentStepContext);

	useEffect(() => {
		if (
			stepsStatus.length === steps.length &&
			stepsStatus.every((result) => result === true)
		) {
			onDone();
		}
	}, [stepsStatus]);

	const validate = async (e) => {
		e.preventDefault();
		const statuses = [];
		for (let i = 0; i < validationSteps.length; i++) {
			let result = await validationSteps[i](e);
			statuses.push(result);
			setCurrentStep(i);
			if(!result) break;
		}

		setStepStatus(statuses);
	};

	return (
		<div className={className}>
			<form
				className="form"
				onSubmit={async (e) => {
					await validate(e);
				}}
			>
				{steps.map((elem, i) => {
					return (
						<div className="stepper stepper--isVertical" key={i}>
							<div>{elem}</div>
							<div
								className={`step step--isVertical ${
									stepsStatus[i] === true
										? 'step--isCurrent'
										: stepsStatus[i] === false
										? 'step--isWrong'
										: 'step--isNext'
								}`}
							>
								{i + 1}
							</div>
						</div>
					);
				})}
				<button className="form__btn" type="submit" value="submit">
					Done!
				</button>
			</form>
		</div>
	);
};

export default Verticalstepper;
