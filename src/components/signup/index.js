import Multiform from '../multiform';
import './style.css';

const Signup = ({ validationSteps, steps, onDone }) => (
	<div className="signup__container">
		<div className="signup__title">Registrati</div>
		<Multiform
			validationSteps={validationSteps}
			onDone={onDone}
			steps={steps}
		/>
	</div>
);

export default Signup;
