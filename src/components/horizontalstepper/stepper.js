import { range } from '../../utils';

const Stepper = ({ numSteps, currentStep, onClick, stepsStatus }) => (
	<div className="stepper">
		{range(numSteps).map((_, i) => (
			<div
				onClick={() => onClick(i)}
				key={i}
				className={`step ${
					i === currentStep ? 'step--isCurrent' : 'step--isNext'
				} ${stepsStatus[i] === false && 'step--isWrong'}`}
			>
				{i + 1}
			</div>
		))}
	</div>
);

export default Stepper;
