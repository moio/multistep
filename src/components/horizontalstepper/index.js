import { useContext, useState } from 'react';
import {
	CurrentStepContext,
	SetCurrentStepContext,
	SetStepStatusContext,
	StepStatusContext,
} from '../../context';
import Multistep from './multistep';

const Horizontalstepper = ({ onDone, validationSteps, steps, className }) => {
	const setStepStatus = useContext(SetStepStatusContext);
	const stepsStatus = useContext(StepStatusContext);
	const setCurrentStep = useContext(SetCurrentStepContext);
	const currentStep = useContext(CurrentStepContext);

	const isStepClickable = (stepNumber) =>
		stepNumber < currentStep || stepsStatus[stepNumber - 1];

	const isDone = () => currentStep === validationSteps.length - 1;
	const nextStep = () => setCurrentStep(currentStep + 1);
	const updateStatus = async (result) => {
		const tmp = await stepsStatus;
		tmp[currentStep] = result;
		setStepStatus(tmp);
	};
	const invalidateNextSteps = async () => {
		const tmp = await stepsStatus;
		setStepStatus(tmp.map((v, i) => (i > currentStep ? null : v)));
	};

	return (
		<Multistep
			className={className}
			stepsStatus={stepsStatus}
			onStepClick={(step) => {
				isStepClickable(step) && setCurrentStep(step);
			}}
			currentStep={currentStep}
			onNext={async (e) => {
				e.preventDefault();
				const result = await validationSteps[currentStep](e);
				await updateStatus(result);
				if (result) {
					isDone() ? onDone(e) : nextStep();
				} else {
					await invalidateNextSteps();
				}
			}}
			steps={steps}
		/>
	);
};

export default Horizontalstepper;
