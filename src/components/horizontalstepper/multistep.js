import Stepper from './stepper';

const Multistep = ({
	className,
	currentStep,
	onNext,
	onStepClick,
	steps,
	stepsStatus,
}) => (
	<div className={className}>
		<Stepper
			stepsStatus={stepsStatus}
			onClick={(i) => {
				if (i < currentStep || stepsStatus[i - 1]) onStepClick(i);
			}}
			numSteps={steps.length}
			currentStep={currentStep}
		/>
		<form
			className="form"
			onSubmit={(e) => {
				onNext(e);
			}}
		>
			{steps[currentStep]}

			<button className="form__btn" type="submit" value="submit">
				{currentStep === steps.length ? 'Done!' : 'Next!'}
			</button>
		</form>
	</div>
);

export default Multistep;
