import { createContext } from 'react';

export const StepStatusContext = createContext(null);
export const SetStepStatusContext = createContext(null);

export const CurrentStepContext = createContext(0);
export const SetCurrentStepContext = createContext(null);
