import { useState } from 'react';
import { api } from './api';
import './App.css';
import Signup from './components/signup';
import Spinner from './components/spinner';
import { toObject } from './utils';

function App() {
	const [isSpinnerVisible, setIsSpinnerVisible] = useState(false);

	const showSpinner = () => {
		setIsSpinnerVisible(true);
	};

	const hideSpinner = () => {
		setIsSpinnerVisible(false);
	};

	const validate = async (e, args) => {
		const formData = new FormData(e.target);
		const toValidate = toObject(args, (name) => formData.get(name));
		showSpinner();
		const { isValid } = await api.validate(toValidate);
		hideSpinner();
		return isValid;
	};

	const firsSubmit = (e) => validate(e, ['firstName', 'username', 'email']);
	const secondSubmit = (e) => validate(e, ['url']);
	const thirdSubmit = (e) => validate(e, ['password']);

	const valSteps = [firsSubmit, secondSubmit, thirdSubmit];

	return (
		<div className="app">
			<Spinner show={isSpinnerVisible} />
			<Signup
				validationSteps={valSteps}
				steps={steps}
				onDone={() => alert('Done!')}
			/>
		</div>
	);
}

const steps = [
	<div className="step-container">
		<input
			className="step-container__input"
			type="text"
			name="firstName"
			placeholder="firstName"
		/>
		<input
			className="step-container__input"
			type="text"
			name="username"
			placeholder="username"
			required
		/>
		<input
			className="step-container__input"
			type="email"
			name="email"
			placeholder="email"
			required
		/>
	</div>,
	<div className="step-container">
		<input
			className="step-container__input"
			type="text"
			name="url"
			placeholder="url"
		/>
	</div>,
	<div className="step-container">
		<input
			required
			type="password"
			className="step-container__input"
			name="password"
			placeholder="password"
		/>
	</div>,
];

export default App;
