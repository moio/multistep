import { stall } from '../utils';

export const api = {
	validate: (form) => {
		return stall({ isValid: chekForm(form) });
	},
};

const chekForm = (form) => {
	if (form.username === 'jo' && form.email === 'jo@example.com') {
		return true;
	} else if (form.url === "" || form.url === 'example.com') {
		return true;
	} else if (form.password === 'secret') {
		return true;
	} else {
		return false;
	}
};
