FROM node:15-alpine3.11

WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
COPY yarn.lock ./
RUN yarn --silent

COPY . ./

# start app
CMD ["yarn", "start"]
